/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __NEWPORT_ENUMS__
#define __NEWPORT_ENUMS__

/**
 * NewportDownloadState:
 * @NEWPORT_DOWNLOAD_STATE_SUCCESS: The download was successfull.
 * @NEWPORT_DOWNLOAD_STATE_IN_PROGRESS: The download is in progress.
 * @NEWPORT_DOWNLOAD_STATE_FAILED: The download has failed.
 * @NEWPORT_DOWNLOAD_STATE_PAUSED_BY_USER: The download is paused by the user.
 * @NEWPORT_DOWNLOAD_STATE_PAUSED_BY_SYSTEM: The download is paused due to no internet or before shutdown done implicitly by newport.
 * @NEWPORT_DOWNLOAD_STATE_CANCELLED: The download is cancelled by the user.
 * @NEWPORT_DOWNLOAD_STATE_QUEUED: The download is queued till present downloads are completed.x
 *
 * Download state.
 */
typedef enum
{
	NEWPORT_DOWNLOAD_STATE_SUCCESS = 0,
	NEWPORT_DOWNLOAD_STATE_IN_PROGRESS,
	NEWPORT_DOWNLOAD_STATE_FAILED,
	NEWPORT_DOWNLOAD_STATE_PAUSED_BY_USER,
	NEWPORT_DOWNLOAD_STATE_PAUSED_BY_SYSTEM,
	NEWPORT_DOWNLOAD_STATE_CANCELLED,
	NEWPORT_DOWNLOAD_STATE_QUEUED
} NewportDownloadState;

#endif
