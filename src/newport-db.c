/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "newport-internal.h"


/* Returns: (transfer full): newly allocated string,has to be freed with g_free() */
static gchar *
newport_db_handler_get_db_path (void)
{
  gchar *db_path = NULL;

  db_path = g_build_filename (g_get_user_data_dir (), "newport", "newport.db",
                              NULL);
  return db_path;
}

static void
newport_db_handler_set_url_info (DownloadUrlInfo *url_info)
{
  gchar *group_name;

  group_name = newport_download_get_group_name (url_info);
  g_return_if_fail (url_info != NULL);
  g_key_file_set_string (pDnlMgrObj.key_file, group_name, "ObjectPath",
                         url_info->object_path);
  g_key_file_set_string (pDnlMgrObj.key_file, group_name, "URLLink",
                         url_info->pUrl);
  g_key_file_set_string (pDnlMgrObj.key_file, group_name, "DownloadPath",
                         url_info->pDownloadPath);
  g_key_file_set_integer (pDnlMgrObj.key_file, group_name, "CurrentState",
                          url_info->u32CurrentDownloadState);
  g_key_file_set_uint64 (pDnlMgrObj.key_file, group_name, "ElapsedTime",
                         url_info->progress.u64ElapsedTime);
  g_key_file_set_uint64 (pDnlMgrObj.key_file, group_name, "DownloadedSize",
                         url_info->progress.u64CurrentSize);
  g_key_file_set_string (pDnlMgrObj.key_file, group_name, "FileName",
                         url_info->pDownloadFileName);
  g_free (group_name);
}

static gboolean
newport_db_handler_save_database (GError **error)
{
  GError *err = NULL;
  gchar *db_path = NULL;
  gboolean ret_value;
  GFile *db_file_obj = NULL;
  GFile *parent_db_obj = NULL;

  db_path = newport_db_handler_get_db_path ();
  db_file_obj = g_file_new_for_path (db_path);
  parent_db_obj = g_file_get_parent (db_file_obj);
  g_file_make_directory_with_parents (parent_db_obj, NULL, &err);
  g_clear_object (&db_file_obj);
  g_clear_object (&parent_db_obj);
  if (err)
    {
      if (!g_error_matches (err, G_IO_ERROR, G_IO_ERROR_EXISTS))
        {
          g_set_error (error, NEWPORT_DOWNLOAD_MANAGER_ERROR,
                       NEWPORT_DOWNLOAD_MANAGER_ERROR_FAILED, "Error creating download database: %s",
                       err->message);
          g_clear_pointer (&err, g_error_free);
          g_free (db_path);
          return FALSE;
        }
      g_clear_pointer (&err, g_error_free);
    }
  ret_value = g_key_file_save_to_file (pDnlMgrObj.key_file, db_path, error);
  g_free (db_path);
  return ret_value;

}

gboolean
initialize_database (GError **error)
{
  GError *err = NULL;
  gchar *db_path = NULL;

  pDnlMgrObj.key_file = g_key_file_new ();
  db_path = newport_db_handler_get_db_path ();
  g_key_file_load_from_file (pDnlMgrObj.key_file, db_path, G_KEY_FILE_NONE, &err);
  g_free (db_path);
  if (err)
    {
      if (!g_error_matches (err, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_NOT_FOUND)
          && !g_error_matches (err, G_FILE_ERROR, G_FILE_ERROR_NOENT))
        {
          g_set_error (error, NEWPORT_DOWNLOAD_MANAGER_ERROR,
                       NEWPORT_DOWNLOAD_MANAGER_ERROR_FAILED,
                       "Error initializing download database: %s",
                       err->message);
          g_error_free (err);
          return FALSE;
        }
      g_error_free (err);
    }
  return TRUE;
}

gboolean
retrieve_contents_from_db (void)
{
  gsize length;
  guint count;
  gchar **group_list = NULL;
  group_list = g_key_file_get_groups (pDnlMgrObj.key_file, &length);
  if (length <= 0)
    {
      g_strfreev(group_list);
      return FALSE;
    }
  for (count = 0; count < length; count++)
    {
      gchar *group_name = NULL;
      DownloadUrlInfo *pUrlInfo;

      pUrlInfo = g_new0 (DownloadUrlInfo, 1);
      initialize_url_info (pUrlInfo);
      pUrlInfo->object_path = g_key_file_get_string (pDnlMgrObj.key_file,
                                                     group_list[count],
                                                     "ObjectPath", NULL);
      pUrlInfo->pUrl = g_key_file_get_string (pDnlMgrObj.key_file, group_list[count],
                                              "URLLink", NULL);
      pUrlInfo->pDownloadPath = g_key_file_get_string (pDnlMgrObj.key_file,
                                                       group_list[count],
                                                       "DownloadPath", NULL);
      pUrlInfo->u32CurrentDownloadState = (guint32) g_key_file_get_integer (
          pDnlMgrObj.key_file, group_list[count], "CurrentState", NULL);
      pUrlInfo->progress.u64ElapsedTime = g_key_file_get_uint64 (pDnlMgrObj.key_file,
                                                             group_list[count],
                                                             "ElapsedTime",
                                                             NULL);
      pUrlInfo->progress.u64CurrentSize = g_key_file_get_uint64 (
          pDnlMgrObj.key_file, group_list[count], "DownloadedSize", NULL);
      pUrlInfo->pDownloadFileName = g_key_file_get_string (pDnlMgrObj.key_file,
                                                           group_list[count],
                                                           "FileName", NULL);
      NEWPORT_DEBUG("url is %s state is %d object path is %s", pUrlInfo->pUrl,
                    pUrlInfo->u32CurrentDownloadState, pUrlInfo->object_path);
      group_name = newport_download_get_group_name (pUrlInfo);
      pUrlInfo->pTempDownloadPath = newport_build_temporary_download_path (
          group_name);
      g_free(group_name);
      //List of all the downloads in from previous ignition cycle
      pDnlMgrObj.pClientDownloadList = g_list_prepend (
          pDnlMgrObj.pClientDownloadList, pUrlInfo);
    }
  g_strfreev (group_list);
  return TRUE;
}

gboolean
store_all_running_downloads_in_db (GError **error)
{
  GList *iter;

   NEWPORT_DEBUG("Storing the download information into database");
   for (iter = pDnlMgrObj.pClientDownloadList; iter!=NULL;
       iter = g_list_next (iter))
   {
       DownloadUrlInfo *pUrlInfo = iter->data;

       if (pUrlInfo->u32CurrentDownloadState == NEWPORT_DOWNLOAD_STATE_IN_PROGRESS)
       {
           pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_PAUSED_BY_SYSTEM;
           newport_db_handler_set_url_info(pUrlInfo);
       }
   }
   return newport_db_handler_save_database(error);
}

gboolean
store_url_data_in_db (DownloadUrlInfo *pUrlInfo, GError **error)
{
  newport_db_handler_set_url_info (pUrlInfo);
  return newport_db_handler_save_database (error);
}

gboolean
dnl_mgr_remove_from_pdi (DownloadUrlInfo *pUrlInfo, GError **error)
{
  gboolean ret_val = FALSE;
  gchar *group_name = newport_download_get_group_name (pUrlInfo);
  ret_val = g_key_file_remove_group (pDnlMgrObj.key_file, group_name, error);
  g_free (group_name);
  if (ret_val)
    return newport_db_handler_save_database (error);
  else
    return FALSE;
}

gchar **
dnl_mgr_get_download_object_paths_from_db (const gchar *app_name)
{
  gchar **group_list = NULL;
  gchar **object_paths = NULL;
  gchar *object_path = NULL;
  gsize length;
  guint count;
  gchar *group_app_name = NULL;

  GPtrArray *ptr_array = NULL;
  group_list = g_key_file_get_groups (pDnlMgrObj.key_file, &length);
 if(length <= 0)
   {
     g_strfreev(group_list);
     return NULL;
   }
  ptr_array = g_ptr_array_new_with_free_func (g_free);
  for (count = 0; count < length; count++)
    {
      group_app_name = g_key_file_get_string (pDnlMgrObj.key_file, group_list[count],
                                              "AppName", NULL);
      if (!g_strcmp0 (group_app_name, app_name))
        {
          object_path = g_key_file_get_string (pDnlMgrObj.key_file, group_list[count],
                                               "ObjectPath", NULL);
          g_ptr_array_add (ptr_array, object_path);
        }

      g_free (group_app_name);
    }
  g_ptr_array_add (ptr_array,NULL);
  object_paths=(gchar **)g_ptr_array_free (ptr_array, FALSE);
  g_strfreev (group_list);
  return object_paths;
}
