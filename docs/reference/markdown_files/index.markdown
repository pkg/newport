# Newport

Newport is download service for downloaing data from internet.

## Overview

Newport is download manager capable of downloading files from the internet
for local storage. Currently the download manager does not specify the type
of files that could be downloaded for the automotive system. The files could
be, audio, video, applications, map data, system images etc.

## Contact

[Mail the maintainers](mailto:maintainers@lists.apertis.org)
